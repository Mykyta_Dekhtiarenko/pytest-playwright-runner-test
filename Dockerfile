FROM python:3.8
WORKDIR /test-automation-playwright/
COPY requirements.txt .
RUN pip install -r requirements.txt
RUN playwright install
RUN playwright install-deps
CMD pytest tests/tests_matches_page.py --alluredir=./allure-results ; allure serve ./allure-results
