import logging
import os

from core.config.common_paths import CommonPaths
from core.config.model.environment_config import EnvironmentConfigDto
from core.utils.file_util import FileUtil


class EnvironmentSingleton(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        """
        Possible changes to the value of the `__init__` argument do not affect
        the returned instance.
        """
        if cls not in cls._instances:
            instance = super().__call__(*args, **kwargs)
            cls._instances[cls] = instance
        return cls._instances[cls]


class EnvLoader(metaclass=EnvironmentSingleton):

    def __init__(self):
        self.env = self.get_run_env()
        print(f"\nAutomation Tests started on Environment: [{self.env.upper()}]")
        self._config = self.__load_config_yaml("config.yaml")

    def __load_config_yaml(self, config_file):
        config_path = os.path.join(CommonPaths.environment_path, self.env, config_file)
        return FileUtil.read_yaml_file(config_path)

    def get_config(self):
        return self._config

    def get_config_value(self, key) -> EnvironmentConfigDto:
        config_data = self._config
        service_config = config_data[key]
        return EnvironmentConfigDto.from_dict(service_config)

    def get_run_env(self):
        file_path = CommonPaths.run_env_config_path
        try:
            run_env = FileUtil.read_yaml_file(file_path)['run_env']
        except Exception as e:
            run_env = 'qa'
            logging.log(level=1, msg=f"Run Env file is empty, setting {run_env} as an default env")
        return run_env
