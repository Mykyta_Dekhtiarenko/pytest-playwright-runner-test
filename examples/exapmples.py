def fibonacci(n):
    fib_list = [0, 1]
    while len(fib_list) < n:
        fib_list.append(fib_list[-1] + fib_list[-2])
    return fib_list



def fib(n):
    arr = [0, 1]
    while len(arr) < n:
        arr.append(arr[-1] + arr[-2])
    return arr

result = fib(8)
print(result)
