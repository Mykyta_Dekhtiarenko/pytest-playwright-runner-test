# my_list = [0, 1, 2, 3, 4, 5, "alex", True, (1, 2), 155]
# jija = {1:"Nikita", 2:"Vika", "chlen":"chel",(1,):"y", True:"Vasya", None:"NONE!!!"}
# my_slice = {k: v.upper() for k, v in jija.items() if str(k).isnumeric()}
# print(my_slice)

def simple_dict_assignment_from_list():
    """
    Create dictionary for list with id, start from id=1
    :return:
    """
    fruits_list = ["apple", "pineapple", "mango", "lemon", "cherry"]
    result_dict = {}
    for i, value in enumerate(fruits_list):
        result_dict[i + 1] = value
    return result_dict


def dict_comprehension():
    """
        Create dictionary for list with id, start from id=1 using dict comprehension
        :return:
        """
    fruits_list = ["apple", "pineapple", "mango", "lemon", "cherry"]
    arr = {index: fruit for index, fruit in enumerate(fruits_list)}
    return arr

if __name__ == '__main__':
    print(dict_comprehension())
