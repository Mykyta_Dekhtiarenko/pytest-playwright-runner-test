import logging

import allure
import pytest
from playwright.sync_api import Playwright
from core.config.common_paths import CommonPaths
from core.utils.environment_loader import EnvLoader


@pytest.fixture
@allure.title("Set Up fixture")
def set_up(playwright: Playwright):
    browser = playwright.chromium.launch(headless=True, slow_mo=500)
    context = browser.new_context(viewport={'width': 1600, 'height': 1250})
    logging.debug('This is an info message')
    page = context.new_page()
    yield page

    png_bytes = page.screenshot()
    allure.attach(
        png_bytes,
        name="full-page",
        attachment_type=allure.attachment_type.PNG
    )
    context.close()


def pytest_runtest_setup(item):
    print("pytest_runtest_setup")


def pytest_addoption(parser):
    env = EnvLoader().get_run_env()
    parser.addoption("--execution_env", action="store", default=env, help="Please provide execution Environment")


def pytest_configure(config):
    env = config.getoption("execution_env")
    if env is None:
        env = 'qa'
    envs = CommonPaths().list_environment_names()
    if env is not None and env not in envs:
        raise ValueError(f"Environment name is not valid. Please choose from: {envs}")
    EnvLoader(env)
