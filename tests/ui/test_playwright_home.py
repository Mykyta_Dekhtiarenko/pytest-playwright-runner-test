import allure
import pytest
from playwright.sync_api import expect


class TestPlaywrightDemo:

    @allure.title("Test Title")
    @allure.description("Test Description")
    @allure.tag("Test Tag1", "Test Tag2")
    @allure.severity(allure.severity_level.CRITICAL)
    @allure.label("owner", "John Doe")
    @allure.issue("JIRA_STORY_ID-123")
    @allure.testcase("TEST_CASE_ID-456")
    @pytest.mark.ui
    def test_runner(self, set_up):
        page = set_up
        with allure.step(f"Test {str(self.test_runner)}"):
            page.goto("https://demo.playwright.dev/todomvc/")
            page.goto("https://demo.playwright.dev/todomvc/#/")
            page.get_by_placeholder("What needs to be done?").click()
            page.get_by_placeholder("What needs to be done?").fill("Hello amigo")
            page.get_by_placeholder("What needs to be done?").press("Enter")
            page.get_by_label("Toggle Todo").check()
            page.get_by_role("link", name="All").click()
            expect(page.get_by_placeholder("What needs to be done?")).to_be_empty()
