from playwright.sync_api import Page

from ui.pages.matches_page import QualityOfLife


class NumbeoHomePage:
    def __init__(self, page: Page):
        self.page = page

    def quality_of_life(self):
        return self.page.get_by_role(role="button", name="Sign in")

    def click_quality_of_life(self):
        self.quality_of_life().click()
        return QualityOfLife